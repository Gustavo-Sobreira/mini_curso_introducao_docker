import React, { useState, useEffect } from 'react';
import './App.css';

function Calendario() {
  const [numeroDiaSelecionado, setNumeroDiaSelecionado] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch('http://localhost:5001/');
        const result = await response.json();
        setNumeroDiaSelecionado(result.day);
      } catch (error) {
        console.error('Erro ao obter número do dia:', error);
      }
    };

    fetchData();
  }, []);


  const renderizarDias = () => {
    const dias = [];
    for (let dia = 1; dia <= 31; dia++) {
      dias.push(
        <div
          key={dia}
          className={`dia ${dia === numeroDiaSelecionado ? 'dia-selecionado' : ''}`}
        >
          {dia}
        </div>
      );
    }
    return dias;
  };

  return (
    <div className="calendario">
      <h1>Calendário 📆</h1>
      <div className="dias">{renderizarDias()}</div>
    </div>
  );
}

export default Calendario;
