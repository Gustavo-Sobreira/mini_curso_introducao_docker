from flask import Flask, jsonify
from flask_cors import CORS
from datetime import datetime
import pytz

app = Flask(__name__)
CORS(app)  

@app.route('/')
def get_dia_atual():
    setar_fuso = pytz.timezone('America/Sao_Paulo')

    horario_completo = datetime.now(setar_fuso)

# Caso queira mudar o dia basta comentar a linha 16 e descomentar a 17, nela coloque o número que quiser entre 1-31
    dia = horario_completo.day
    #dia = 5

    return jsonify({'day': dia}), 200

if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', port=5000)
