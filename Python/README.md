# Importações
Linha 1-4
    Importações de bibliotecas que facilitam a construção do código.


# Criação da Aplicação Flask
Linha 6-7
    Cria uma instância da aplicação Flask.
    CORS(app) adiciona suporte para CORS a todas as rotas da aplicação.

# Rota
Linha 9-10
    Cria o endpoint na URL, nesse caso o '/', mas poderia ser qualquer coisa, como um '/dia', etc...

# Lógica de código
Linha 11-16
    1° Mostro ao computador o fuso horário que ele deve buscar a data.
    
    2° Busco a data atual com ajuda da biblioteca 'datetime' que importei no começo do código.
    
    3° Filtro a data, para pegar apenas o dia e o coloco em uma variável com nome 'dia'

    4° Retorno o resultado da variável 'dia'

# Execução da Aplicação:

    Inicia a aplicação Flask se o script for executado diretamente